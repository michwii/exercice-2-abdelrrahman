# Exercice  2 Abdelrrahman


## Prérequis

Avoir une VM de type AD DC.
L'instance de l'AD doit être dans un subnet à part

## Inputs

1 - Nom de la VM <br/>
2 - Subnet de destination<br/>
3 - SKU de la VM<br/>
4 - OS (uniquement des OS Windows)<br/>
5 - DataCenter<br/>
6 - OU et DC où join la VM<br/>

## Ce que fait le pipeline
Utiliser Azure CLI à l'intérieur du pipeline.<br/>
1 - Vérifier que le VNET de l'AD et son subnet existe<br/>
2 - Créer un subnet dans le même VNET que l'AD si non existant<br/>
3 - Faire en sorte que le DNS par défaut du VNET pointe vers l'IP de l'AD<br/>
4 - Créer une VM avec le nom demandé si non déjà existante<br/>
5 - Faire un join de la VM à l'AD<BR/>
Essayer de découper le pipeline en plein de petit job.<br/>
1 job = 1 action bien spécifique.<br/>
Essayer de paralléliser ce qui est parallélisable.<br/>

## Outputs

Au format JSON<br/>
1 - ID Azure de la VM<br/>
2 - Adresse IP<br/>
3 - Login local (autogénéré) <br/>
4 - Password local (autogénéré)<br/>


## Acceptance  critérias

Si tu es capable de te connecter à la VM avec un compte présent dans l'AD tu peux considérer que l'exercice est terminé.
